server {
  listen 80 ;
  listen [::]:80 ;
  root /usr/share/nginx/html/hackathon;
  index index.html;
  server_name example.com www.example.com;



    location /Team1/images/ {

         autoindex on;

    }

    location /Team1/pdf/ {
        autoindex on;
    }
     location /Team2/images {

         autoindex on;

    }

    location /Team2/pdf/ {
        autoindex on;
    }
  location / {
    try_files $uri $uri/ =404;
  }
}
