"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"


import json
import logging
from json import JSONDecodeError
# from flask import Flask
# from flask import request
from flask import Flask,render_template,request
from pprint import pprint
import requests
from requests.exceptions import HTTPError

# Following import are required for Xpresso. Do not remove this.
from xpresso.ai.core.logging.xpr_log import XprLogger

config_file = 'config/dev.json'

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="flask_app",
                   level=logging.INFO)


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app



__finance_url__  = "http://172.16.6.51:31647/search"
__healthcare_url__  = "http://172.16.6.51:31795/predict"
plan_type_list = ['HMO','PPO']
# plan_name_list = ['Ambetter Platinum 1','California Value Trust','ConnectorCare 1','ConnectorCare 2','Ambetter Gold 4']

plan_name_dict = {
    # 'California Value Trust':'California Value Trust',

    'Ambetter from Coordinated Care Corporation: Ambetter Bronze ': 'Ambetter Bronze',
    'Ambetter from Coordinated Care Corporation: Ambetter Silver': 'Ambetter Silver',
    'Ambetter from Coordinated Care Corporation: Ambetter Gold': 'Ambetter Gold',

    'Ambetter Platinum 2': 'Ambetter Platinum 2',
    'Ambetter Platinum 1': 'Ambetter Platinum 1',

    'Ambetter Gold 4': 'Ambetter Gold 4',
    'Ambetter Gold 3': 'Ambetter Gold 3',
    'Ambetter Gold 2': 'Ambetter Gold 2',

    'Ambetter Silver 5': 'Ambetter Silver 5',
    'Ambetter Silver 4': 'Ambetter Silver 4',
    'Ambetter Silver 3': 'Ambetter Silver 3',

    'Ambetter Silver 1': 'Ambetter Silver 1',

    'Ambetter Bronze 1': 'Ambetter Bronze 1',

    'Ambetter Bronze 4': 'Ambetter Bronze 4',
    'Ambetter Bronze 3': 'Ambetter Bronze 3',

    'Ambetter Gold 4 + Vision': 'Ambetter Gold 4 + Vision',
    'Ambetter Gold 2 + Vision + Adult Dental': 'Ambetter Gold 2 + Vision + Adult Dental',
    'Ambetter Gold 2 + Vision': 'Ambetter Gold 2 + Vision',
    'Ambetter Gold 1': 'Ambetter Gold 1',
    'Ambetter Silver 5 + Vision': 'Ambetter Silver 5 + Vision',
    'Ambetter Silver 5 + Vision + Adult Dental': 'Ambetter Silver 5 + Vision + Adult Dental',
    'Ambetter Silver 4 + Vision': 'Ambetter Silver 4 + Vision',
    'Ambetter Silver 4 + Vision + Adult Dental': 'Ambetter Silver 4 + Vision + Adult Dental',
    'Ambetter Silver 3 + Vision + Adult Dental': 'Ambetter Silver 3 + Vision + Adult Dental',
    'Ambetter Silver 3 + Vision': 'Ambetter Silver 3 + Vision',
    'Ambetter Silver 1 + Vision': 'Ambetter Silver 1 + Vision',
    'Ambetter Silver 1 + Vision + Adult Dental': 'Ambetter Silver 1 + Vision + Adult Dental',
    'Ambetter Bronze 4 + Vision': 'Ambetter Bronze 4 + Vision',
    'Ambetter Bronze 4 + Vision + Adult Dental': 'Ambetter Bronze 4 + Vision + Adult Dental',
    'Ambetter Bronze 3 + Vision': 'Ambetter Bronze 3 + Vision',
    'Ambetter Bronze 3 + Vision + Adult Dental': 'Ambetter Bronze 3 + Vision + Adult Dental',
    'Ambetter Bronze 1 + Vision + Adult Dental': 'Ambetter Bronze 1 + Vision + Adult Dental',
    'Ambetter Bronze 1 + Vision': 'Ambetter Bronze 1 + Vision',


    'Ambetter from MHS: Ambetter Gold 4 + Vision + Adult Dental': 'Ambetter Gold 4 + Vision + Adult Dental',
    'Ambetter from MHS: Ambetter Silver 5 + Vision + Adult Dental':'Ambetter Silver 5 + Vision + Adult Dental',
    'Ambetter from MHS: Ambetter Silver 4': 'Ambetter Silver 4',
    'Ambetter from MHS: Ambetter Silver 4 + Vision + Adult Dental': 'Ambetter Silver 4 + Vision + Adult Dental',
    'Ambetter from MHS: Ambetter Silver 4 + Vision': 'Ambetter Silver 4 + Vision',
    'Ambetter from MHS: Ambetter Bronze 4 + Vision': 'Ambetter Bronze 4 + Vision',
    'Ambetter from MHS: Ambetter Bronze 3 + Vision + Adult Dental':'Ambetter Bronze 3 + Vision + Adult Dental',
    'Ambetter from MHS: Ambetter Bronze 3': 'Ambetter Bronze 3',
    'Ambetter from MHS: Ambetter Bronze 1': 'Ambetter Bronze 1',
    'Ambetter from MHS: Ambetter Bronze 1 + Vision + Adult Dental :': 'Ambetter Bronze 1 + Vision + Adult Dental',


    'Ambetter from MHS: Ambetter Silver 1 + Vision + Adult Dental': 'Ambetter Silver 1 + Vision + Adult Dental',
    'Ambetter from MHS: Ambetter Silver 3 + Vision + Adult Dental': 'Ambetter Silver 3 + Vision + Adult Dental',
    'Ambetter from MHS: Ambetter Silver 5 + Vision': 'Ambetter Silver 5 + Vision',
    'Ambetter from MHS: Ambetter Bronze 4': 'Ambetter Bronze 4',
    'Ambetter from MHS: Ambetter Silver 3': 'Ambetter Silver 3',
    'Ambetter from MHS: Ambetter Silver 5': 'Ambetter Silver 5',
    'Ambetter from MHS: Ambetter Gold 2 + Vision': 'Ambetter Gold 2 + Vision',
    'Ambetter from MHS: Ambetter Gold 4 + Vision': 'Ambetter Gold 4 + Vision',
    'Ambetter from MHS: Ambetter Silver 1 + Vision': 'Ambetter Silver 1 + Vision',
    'Ambetter from MHS: Ambetter Bronze 1 + Vision': 'Ambetter Bronze 1 + Vision',

    'Ambetter from MHS: Ambetter Gold 2': 'Ambetter Gold 2',
    'Ambetter from MHS: Ambetter Silver 1': 'Ambetter Silver 1',
    'Ambetter from MHS: Ambetter Bronze 3 + Vision': 'Ambetter Bronze 3 + Vision',
    'Ambetter from MHS: Ambetter Gold 4': 'Ambetter Gold 4',

    'Ambetter from MHS: Ambetter Gold 2 + Vision + Adult Dental': 'Ambetter Gold 2 + Vision + Adult Dental',
    'Ambetter from MHS: Ambetter Silver 3 + Vision': 'Ambetter Silver 3 + Vision',
    'Ambetter from MHS: Ambetter Bronze 4 + Vision + Adult Dental ': 'Ambetter Bronze 4 + Vision + Adult Dental',

    'ConnectorCare 1': 'ConnectorCare 1',
    'ConnectorCare 2': 'ConnectorCare 2',
    'ConnectorCare 3': 'ConnectorCare 3',

    #                           PPO

    'Ambetter of Arkansas: Ambetter Silver 1 + Vision + Dental': 'Ambetter Silver 1 + Vision + Dental ',
    'Ambetter of Arkansas: Ambetter Silver 2 + Vision + Adult Dental': 'Ambetter Silver 2 + Vision + Adult Dental',
    'Ambetter of Arkansas: Ambetter Bronze 1': 'Ambetter Bronze 1',
    'Ambetter of Arkansas: Ambetter Bronze 2': 'Ambetter Bronze 2',
    'Ambetter of Arkansas: Ambetter Silver 1': 'Ambetter Silver 1',
    'Ambetter of Arkansas: Ambetter Bronze 1 + Vision + Adult Dental': 'Ambetter Bronze 1 + Vision + Adult Dental',
    'Ambetter of Arkansas: Ambetter Gold 2 + Vision + Adult Dental': 'Ambetter Gold 2 + Vision + Adult Dental',
    'Ambetter of Arkansas: Ambetter Silver 2 + Vision + Dental': 'Ambetter Silver 2 + Vision + Dental',
    'Ambetter of Arkansas: Ambetter Gold 1': 'Ambetter Gold 1',
    'Ambetter of Arkansas: Ambetter Bronze 2 + Vision + Adult Dental': 'Ambetter Bronze 2 + Vision + Adult Dental',
    'Ambetter of Arkansas: Ambetter Gold 1 + Vision': 'Ambetter Gold 1 + Vision',
    'Ambetter of Arkansas: Ambetter Bronze 1 + Vision': 'Ambetter Bronze 1 + Vision',
    'Ambetter of Arkansas: Ambetter Gold 1 + Vision + Adult Dental': 'Ambetter Gold 1 + Vision + Adult Dental',
    'Ambetter of Arkansas: Ambetter Silver 2': 'Ambetter Silver 2',
    'Ambetter of Arkansas: Ambetter Bronze 2 + Vision': 'Ambetter Bronze 2 + Vision',
    'Ambetter of Arkansas: Ambetter Silver 2 + Vision': 'Ambetter Silver 2 + Vision',
    'Ambetter of Arkansas: Ambetter Silver 1 + Vision + Adult Dental': 'Ambetter Silver 1 + Vision + Adult Dental ',
    'Ambetter of Arkansas: Ambetter Silver 1 + Vision': 'Ambetter Silver 1 + Vision',
    'Ambetter of Arkansas: Ambetter Gold 2 + Vision': 'Ambetter Gold 2 + Vision',
    'Ambetter of Arkansas: Ambetter Gold 2': 'Ambetter Gold 2',

}


# print(__finance_url__  )
app = create_app()


@app.route('/')



@app.route("/finance", methods = ['POST','GET'])

def finance():
    # que = ''
    if request.method == 'POST':
        query  = request.form['query']
        # print(que)
        result = process_finance_api(query)
        return render_template('finance_res.html', data=request.form, result = result)
    return render_template('finance.html')

@app.route("/healthcare", methods = ['POST','GET'])

def healthcare():
    if request.method == 'POST':
        query = request.form['query']
        plan_name = request.form['plan_name']
        plan_type = request.form['plan_type']

        # print(query)
        # print(plan_name)
        # print(plan_type)

        result = process_healthcare_api(query,plan_name,plan_type)
        p=sorted(result, key = lambda i: i['Answer'],reverse= False)
        q = sorted(result, key = lambda i: i['Answer'],reverse= True)
        # print(result[0]['Url'].split('/')[-1])
        return render_template('healthcare_res.html',plan_type_list= plan_type_list,plan_name_dict =plan_name_dict ,data = request.form, result = result,p=p,q=q,fetch=True)

    else:
        return render_template('healthcare.html',plan_type_list= plan_type_list,plan_name_dict =plan_name_dict)


def process_finance_api(query):
    """
    :param query:
    :return:
    """
    try:
        data = {
            'query':query
        }

        response = requests.post(url=__finance_url__,json= data)
        response.raise_for_status()
        datarec = response.json()
        # pprint(datarec['results'])
        return datarec['results']

    except HTTPError as e:
        print(e)



def process_healthcare_api(query,plan_name,plan_type):
    """
    :param query:
    :param plan_name:
    :param plan_type:
    :return:
    """

    try:
        data = {"input":
                    { "plan_name": plan_name,
                        "plan_type": plan_type,
                        "query": query,
                        "result_count" : 10
                    }
                }

        response = requests.post(url=__healthcare_url__,json= data)
        response.raise_for_status()
        datarec = response.json()
        result = datarec['results']['results']
        # pprint(datarec['results']['results'])

        return result



    except HTTPError as e:
        print("error found")
        print(e)



# def hello_world():
#     """
#     Send response to Hello World
#     """
#
#     logger.info("Received request from {}".format(request.remote_addr))
#     try:
#         logger.info("Processing the request")
#         cfg_fs = open(config_file, 'r')
#         config = json.load(cfg_fs)
#         project_name = config["project_name"]
#         logger.info("Request Processing Done")
#         logger.info("Sending Response to {}".format(request.remote_addr))
#         return '<html><body><b>Hello World from {}!</b></body></html>'.format(
#             project_name
#         )
#     except (FileNotFoundError, JSONDecodeError):
#         logger.error("Request Processing Failed")
#         logger.info("Sending Default Response")
#         return '<html><body><b>Hello World!</b></body></html>'


if __name__ == '__main__':
    # app.run(debug= True)
    app.run("0.0.0.0")
